// Módulos
var gulp = require('gulp'),
	plumber = require('gulp-plumber'),
	jade = require('gulp-jade'),
	prettify = require('gulp-prettify'),
	connect = require('gulp-connect'),
	path = require('../../gulpconfig').html;

// Tarea | Crear templates HTML
gulp.task('templates', function() {
	return gulp.src(path.in)
		.pipe(plumber())
		.pipe(jade())
		.pipe(prettify({indent_size: 4}))
		.pipe(gulp.dest(path.out))
		.pipe(connect.reload());
});