// Módulos
var gulp = require('gulp'),
	path = require('../../gulpconfig');



// Tarea | Angular
gulp.task('angular', function() {
	gulp.src(path.js.angular)
	.pipe(gulp.dest(path.js.vendors));
});

// Tarea | GSAP | Timeline Lite
gulp.task('timelinelite', function() {
	gulp.src(path.gsap.TimelineLite)
	.pipe(gulp.dest(path.js.vendors));
});

// Tarea | GSAP | Timeline Max
gulp.task('timelinemax', function() {
	gulp.src(path.gsap.TimelineMax)
	.pipe(gulp.dest(path.js.vendors));
});

// Tarea | GSAP | Tween Lite
gulp.task('tweenlite', function() {
	gulp.src(path.gsap.TweenLite)
	.pipe(gulp.dest(path.js.vendors));
});

// Tarea | GSAP | Tween Max
gulp.task('tweenmax', function() {
	gulp.src(path.gsap.TweenMax)
	.pipe(gulp.dest(path.js.vendors));
});


// Tarea | Copia librerías GSAP
gulp.task('gsap', ['timelinelite', 'tweenlite']);

// Tarea | Copia los frameworks
gulp.task('frameworks', ['angular']);