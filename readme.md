# Prueba ETS Fintech

Prueba técnica para ETS Fintech desarrollada por [Álvaro Möo](http://www.alvaromoo.com/)


## Instalación

Es necesario tener instalado [Node.js](https://nodejs.org/) y [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

También necesitarás tener Gulp instalado globalmente en tu sistema:
```sh
$ npm i -g gulp
```

Clona el repositorio desde [SourceTree](https://www.sourcetreeapp.com/) o a través de la consola.
```sh
$ git clone https://bitbucket.org/alvaromoo/ets_fintech
```

Una vez clonado, desde la raíz, instalamos los módulos.
```sh
$ npm install
```

Finalmente podemos arrancar la aplicación.
```sh
$ gulp
```


## ToDo
- Fecha y hora de comentarios
- Edición de comentarios
- Asociación de comentarios a ficha individual
- Charts para visualización de valor de activos



## Versión
1.0