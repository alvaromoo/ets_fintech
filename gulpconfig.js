/* ----- *
 * RUTAS *
 * ----- */

// Rutas | Base
var basePaths = {
	dev: 'dev/',
	app: 'app/',
	modules: 'node_modules/'
};

// Rutas | Específicas
module.exports = {
	html: {
		in: basePaths.dev + 'jade/**/!(_)*.jade',
		watch: basePaths.dev + 'jade/**/*.jade',
		out: basePaths.app
	},
	sass: {
		in: basePaths.dev + 'sass/**/*.scss',
		out: basePaths.app + '',
		dir: basePaths.dev + 'sass/'
	},
	js: {
		in: basePaths.dev + 'js/**/*.js',
		out: basePaths.app + '',
		vendors: basePaths.app + 'js/vendors',
		angular: basePaths.modules + 'angular/angular.min.js',
		datejs: basePaths.modules + ''
	},
	gsap: {
		TimelineLite: basePaths.modules + 'gsap/src/minified/TimelineLite.min.js',
		TimelineMax: basePaths.modules + 'gsap/src/minified/TimelineMax.min.js',
		TweenLite: basePaths.modules + 'gsap/src/minified/TweenLite.min.js',
		TweenMax: basePaths.modules + 'gsap/src/minified/TweenMax.min.js'
	},
	images: {
		in: basePaths.dev + 'images/**',
		out: basePaths.app + 'images/'
	},
	fonts:  {
		in: basePaths.dev + 'fonts/**',
		out: basePaths.app + 'fonts/'
	},
	server: {
		host: 'http://localhost:8080',
		browser: 'chrome',
		out: basePaths.app
	}
}