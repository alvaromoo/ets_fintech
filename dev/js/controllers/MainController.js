// Controlador de activos
app.controller('etsController', function ($scope, $http){

	$http({
		url: 'http://jsonstub.com/etsfintech/symbols',
		method: 'GET',
		dataType: 'json',
		data: '',
		headers: {
			'Content-Type': 'application/json',
			'JsonStub-User-Key': '9facef2e-9583-4a83-9f08-c87159f1c113',
			'JsonStub-Project-Key': '6ed070c1-b334-4612-8fa8-169c5e45baef'
		}
		}).success(function (data, status, headers, config) {
			
			// console.log(JSON.stringify(data, null, 4));

			$scope.activos = data;

			$scope.toggleLoadingClass();

			$scope.currentID  = '';

			$scope.setID = function(id){

				// ID actual
				$scope.currentID = id;

				// Clase de ID actual
				$scope.activeClass = 'id-' + $scope.currentID + '-active';

				// Request
				$http({
					url: 'http://jsonstub.com/etsfintech/symbols/' + id,
					method: 'GET',
					dataType: 'json',
					data: '',
					headers: {
						'Content-Type': 'application/json',
						'JsonStub-User-Key': '9facef2e-9583-4a83-9f08-c87159f1c113',
						'JsonStub-Project-Key': '6ed070c1-b334-4612-8fa8-169c5e45baef'
					}
					}).success(function (data, status, headers, config) {
						
						// console.log(JSON.stringify(data, null, 4));

						$scope.ficha = data;

						$scope.toggleLoadingClass();

						// Load More
						var num = 7;
						$scope.limit = num;

						$scope.loadMore = function() {
							if(num < $scope.ficha.prices.length){
								$scope.limit = $scope.limit + num;
							}
						}
					});
			}
		});

	// Toggle Class | Ventana modal
	$scope.modalClass = '';
	$scope.toggleModalClass = function(){
		if ($scope.modalClass === '')
			$scope.modalClass = 'modal-open';
		else
			$scope.modalClass = '';
	};

	// Toggle Class | Carga
	$scope.loadingClass = 'loading';
	$scope.toggleLoadingClass = function(){
		if ($scope.loadingClass === '')
			$scope.loadingClass = 'loading';
		else
			$scope.loadingClass = '';
	};
});




// Comentarios
app.controller('FrmController', function ($scope){

	$scope.comment = {
		txt: [],
		date: []
	};


	$scope.btn_add = function() {
		if($scope.txtcomment !=''){
			$scope.comment.txt.push($scope.txtcomment);
			$scope.txtcomment = "";

			console.log('La fecha: ' + Date.today());

			// $scope.comment.date.push( Date.today() );
		}

		// console.log('Desde el controlador de comentarios: ' + $scope.currentID);
	}

	$scope.remItem = function($index) {
		$scope.comment.txt.splice($index, 1);
	}

});

app.filter('reverse', function() {
	return function(items) {
		if (!items || !items.length) { return; }
		return items.slice().reverse();
	};
});

app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});



/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2013 Thom Seddon
 * Copyright (c) 2010 Google
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * Adapted from: http://code.google.com/p/gaequery/source/browse/trunk/src/static/scripts/jquery.autogrow-textarea.js
 *
 * Works nicely with the following styles:
 * textarea {
 *  resize: none;
 *  word-wrap: break-word;
 *  transition: 0.05s;
 *  -moz-transition: 0.05s;
 *  -webkit-transition: 0.05s;
 *  -o-transition: 0.05s;
 * }
 *
 * Usage: <textarea auto-grow></textarea>
 */

app.directive('autoGrow', function() {
  return function(scope, element, attr){
    var minHeight = element[0].offsetHeight,
      paddingLeft = element.css('paddingLeft'),
      paddingRight = element.css('paddingRight');

    var $shadow = angular.element('<div></div>').css({
      position: 'absolute',
      top: -10000,
      left: -10000,
      width: element[0].offsetWidth - parseInt(paddingLeft || 0) - parseInt(paddingRight || 0),
      fontSize: element.css('fontSize'),
      fontFamily: element.css('fontFamily'),
      lineHeight: element.css('lineHeight'),
      resize:     'none'
    });
    angular.element(document.body).append($shadow);

    var update = function() {
      var times = function(string, number) {
        for (var i = 0, r = ''; i < number; i++) {
          r += string;
        }
        return r;
      }

      var val = element.val().replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/&/g, '&amp;')
        .replace(/\n$/, '<br/>&nbsp;')
        .replace(/\n/g, '<br/>')
        .replace(/\s{2,}/g, function(space) { return times('&nbsp;', space.length - 1) + ' ' });
      $shadow.html(val);

      element.css('height', Math.max($shadow[0].offsetHeight + 10 /* the "threshold" */, minHeight) + 'px');
    }

    element.bind('keyup keydown keypress change', update);
    update();
  }
});